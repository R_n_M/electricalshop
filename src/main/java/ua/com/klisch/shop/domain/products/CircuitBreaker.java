package ua.com.klisch.shop.domain.products;

import static ua.com.klisch.shop.domain.Category.CIRCUITBREAKER;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.com.klisch.shop.domain.Product;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class CircuitBreaker extends Product {

    private int amperage;

    public CircuitBreaker() {
        super(CIRCUITBREAKER);
    }

}
