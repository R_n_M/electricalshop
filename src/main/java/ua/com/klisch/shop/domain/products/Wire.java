package ua.com.klisch.shop.domain.products;

import static ua.com.klisch.shop.domain.Category.WIRE;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.com.klisch.shop.domain.Product;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Wire extends Product {

    private int crossSection;

    private int amperage;

    public Wire() {
        super(WIRE);
    }

}
