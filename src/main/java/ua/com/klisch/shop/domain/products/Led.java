package ua.com.klisch.shop.domain.products;

import static ua.com.klisch.shop.domain.Category.LED;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.com.klisch.shop.domain.Product;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Led extends Product {

    private String viewCristal;

    private int lightFlow;

    public Led() {
        super(LED);
    }

}
