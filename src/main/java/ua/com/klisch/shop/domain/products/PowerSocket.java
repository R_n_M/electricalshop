package ua.com.klisch.shop.domain.products;

import static ua.com.klisch.shop.domain.Category.POWERSOCKET;
import lombok.Data;
import lombok.EqualsAndHashCode;
import ua.com.klisch.shop.domain.Product;

import javax.persistence.Entity;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class PowerSocket extends Product {

    private String color;

    public PowerSocket() {
        super(POWERSOCKET);
    }

}
