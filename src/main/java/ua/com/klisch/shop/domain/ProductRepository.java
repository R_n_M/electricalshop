package ua.com.klisch.shop.domain;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class ProductRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Product sava(Product product){
        entityManager.persist(product);
        return product;
    }

    @Transactional
    public Product remove(Product product){
        entityManager.remove(product);
        return product;
    }

    @Transactional
    public Product update(Product product){
        entityManager.refresh(product);
        return product;
    }

    public List<Product> findByCategory(String category){
        return entityManager.createNamedQuery(Product.FIND_BY_CATEGORY,Product.class)
                .setParameter("category",category).getResultList();
    }

    public Product findByName(String category){
        return entityManager.createNamedQuery(Product.FIND_BY_NAME,Product.class)
                .setParameter("category",category).getSingleResult();
    }

}
