package ua.com.klisch.shop.domain;

public enum Category {
    WIRE,
    POWERSOCKET,
    CIRCUITBREAKER,
    RELAY,
    LED;
}
