package ua.com.klisch.shop.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;

@Data
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries(value = {
        @NamedQuery(name = Product.FIND_BY_CATEGORY, query = "select a from product a where a.category = :category"),
        @NamedQuery(name = Product.FIND_BY_NAME, query = "select a from Product a where a.name = :name")
})
public class Product implements java.io.Serializable {

    public static final String FIND_BY_CATEGORY = "Account.findByCategory";
    public static final String FIND_BY_NAME = "Account.findByName";

    @Id
    @GeneratedValue
    private int id;

    private int price;

    private String name;

    private String manufacturer;

    @Enumerated(EnumType.STRING)
    private final Category category;

}
